@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Follower</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
				    <div class='row'>
				    <input type='text' style="width: 83%;float: left;" class='form-control' id='username' value='' placeholder='Enter the github username' />
					<button id='submit'   class='btn btn-info float-right'>submit</button>
					</div>
					<br>
					 <div class='row' id='Follower' style='display:none'>
					<h5>Follower:</h5>
					<table class="table">
					<thead>
						<tr>
							<th scope="col">Username</th>
						</tr>
					</thead>
					<tbody id='response'>
						<tr >
							
						</tr>
					</tbody>
					<table>
					
					<button id='load-more' style='display:none'  class='btn btn-info float-right' data-page='1'>Load More</button>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    $("#submit").click(function(){
		var username = $('#username').val();
       if(username.length >= 4){
		    $.get("{{url('followers')}}/"+username+'/1', function(response){
				if(response.status =='success'){
					$('#load-more').data('page',response.nextpage);
					if(response.data.length > 0){
						console.log(response.data);
						$('#response').empty();
					response.data.forEach(function(item){
						$('#response').append("<tr><td >"+item.login+"</td></tr>");
					});
					}else{
						$('#response').html("<tr><td >No Follower found</td></tr>");
					}
					if(response.nextpage != '-1'){
						
						$('#load-more').show();
					}
				}  $('#Follower').show();        
        });
		   
	   }
    });
	$("#load-more").click(function(){
		$('#load-more').hide();
		var username = $('#username').val();
		var page = $('#load-more').data('page');
       if(username.length >= 4){
		    $.get("{{url('followers')}}/"+username+'/'+page, function(response){
				if(response.status =='success'){
					$('#load-more').data('page',response.nextpage);
					if(response.data.length > 0){
					response.data.forEach(function(item){
						$('#response').append("<tr><td >"+item.login+"</td></tr>");
					});
					}else{
						$('#response').html("<tr><td>No Follower found</td></tr>");
					}
					if(response.nextpage != '-1'){						
						$('#load-more').show();
					}
					
				}            
        });
		   
	   }
    });
});
</script>
@endsection
