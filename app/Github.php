<?php

namespace App;

use GuzzleHttp\Client;

class Github
{
    private $api;
    private $company;
    private $base_uri;

    public function __construct()
    {
        $this->base_uri = 'https://api.github.com';
        $this->api = new Client([
            'base_uri' => $this->base_uri,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
    }

    public function followers($username,$page=1)
    {
        $response = $this->api->get( '/users/'.$username.'/followers?page='.$page);
        $result = json_decode($response->getBody()->getContents(), true);
        return $result;
    }
   
}
