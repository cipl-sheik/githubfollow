<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Github;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
	public function followers($username,$page=1)
    {
		$res['status']='success';
		$res['page']=$page;
		$next=$page+1;
		$res['nextpage']=(count((new Github)->followers($username,$next))>0)?$next:'-1';
		$res['data']=(new Github)->followers($username,$page);
        return $res;
    }
}
